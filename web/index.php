<?php

use \crazyday\models\categorie ;
use \crazyday\models\Item;
use \crazyday\controler\ListParticipationControler;
use \crazyday\controler\ListCreateControler;
use \crazyday\controler\CompteControler;
use \crazyday\controler\ItemControler;
use \crazyday\controler\ImageControler;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';


session_start();

$db = new DB();
$db->addConnection( [
 'driver' => 'mysql',
 'host' => 'localhost',
 'database' => 'daubenfe6u',
 'username' => 'daubenfe6u',
 'password' => '12345',
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$test = new \crazyday\Test();
$c2 = new \crazyday\controler\CompteControler();
//echo "coucou";


// $app->post('/',function(){
//   $c2 = new CompteControler();
//   $c2->formulaireConnexion();
//
// });

// $test = categorie ::get();
// var_dump($test);

$app->get('/',function(){
  $c2 = new \crazyday\controler\CompteControler();
  $c2->afficherButtonsAccueil();
})->name('accueil');


$app->post('/',function(){
  if (isset($_POST['deconnexion_post_name'])){
    session_destroy();
  }
  $c2 = new CompteControler();
  $c2->afficherButtonsAccueil();
});

$app->get('/authentification',function(){
  $c = new CompteControler();
  $c->afficherLesComptes();
});

$app->post('/authentification',function(){
  $c = new CompteControler();
  $c->authentifierCompte();
});

$app->get('/hello/world',function(){
  echo "hello world !";
});


$app->post('/categories',function(){
  $c2 = new ListCreateControler();
  $c2->creercategorie();

});

$app->get('/categories',function(){
  $c2 = new ListParticipationControler();
  $c2->affichercategorieDescategories();
});

$app->get('/categoriesCreateur',function(){
  $c = new ListParticipationControler();
  $c->affichercategoriesDuCreateur();
});

$app->get('/categorieCreateur/:idcategorie', function($idcategorie){
  $c = new ItemControler();
  $c->afficherItems_Avec_Button_Image($idcategorie);
});

$app->get('/categorie/:idcategorie',function($idcategorie){
  $c = new ListParticipationControler();
  $c->afficherItemsDeLacategorie($idcategorie);
});

$app->post('/categorie/:idcategorie',function($idcategorie){
  $c = new ListCreateControler();
  $c->ajouterItem($idcategorie);
});

$app->get('/item/reserve',function(){
    $c = new ListParticipationControler();
    $c->afficherItemReserve();
});

$app->get('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->afficherItem($id);
});

$app->get('/item/:id/img/ajouter',function($iditem){
  $c = new ImageControler();
  $c->formulaire_ajout_image_on_item($iditem);
});

$app->post('/item/:id/img/ajouter',function($iditem){
  $c = new ImageControler();
  $c->afficher_image_ajout($iditem);
});

$app->get('/item/:id/img/modifier',function($iditem){
  $c = new ImageControler();
  $c->formulaire_modif_image_on_item($iditem);
});

$app->post('/item/:id/img/modifier',function($iditem){
  $c = new ImageControler();
  $c->afficher_image_modif($iditem);
});

$app->get('/item/modifier/:id',function($iditem){
  $c = new ItemControler();
  $c->formulaire_modif_item($iditem);
});

$app->post('/item/modifier/:id',function($iditem){
  $c = new ItemControler();
  $c->afficher_modif_item($iditem);
});

$app->post('/categorieCreateur/:idcategorie', function($idcategorie ){
  if(isset($_POST['supprimer_img_item'])){
    $c = new ImageControler();
    $c->afficher_image_supprimer($idcategorie );
  }
  else if(isset($_POST['ajouter_item_categorieCrea'])){
    $c = new ItemControler();
    $c->ajouter_item_categorieCreateur($idcategorie );
  }else if(isset($_POST['supprimer_item_categorieCrea'])){
      $c = new ItemControler();
      $c->supprimer_item_categorieCrea($idcategorie );
  }else{
    $v = new VueErreur();
    $v->render(0);
  }
});

$app->post('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->reserver($id);
});

$app->post('/item/suppr/:id',function($id){
    $c = new ListParticipationControler();
    $c->supprimerItemDecategorie($id);
});

 $app->post('/compte', function(){
   $c = new CompteControler();
   $c->creerCompteControl();
 });

 $app->get('/compte/:idcompte', function($idcompte){
   $c = new CompteControler();
   $c->sauvegarderConnexion($idcompte);
 });

 $app->get('/inscription', function(){
   $c = new CompteControler();
   $c->afficherLesComptes();
 });

 $app->post('/categories/modif',function(){
   $c2 = new ListParticipationControler();
   $c2->modifiercategorie($_POST['numerocategorie']);
 });

 $app->post('/categories/supp',function(){
   $c3 = new ListParticipationControler();
   $c3->supprimercategorie($_POST['numerocategorie']);
 });

$app->get('/parametres',function(){
  $c = new CompteControler();
  $c->afficher_Modifications();
});

$app->post('/parametres',function(){
  $c = new CompteControler();
  $c->post_Modifications();
});

$app->get('/deconnection',function(){
  $c = new CompteControler();
  $c->deconnecter_utilisateur();
});


$app->run();

// session_destroy();

<?php
namespace crazyday\models;

class Categorie extends \Illuminate\Database\Eloquent\Model{

  protected $table = 'categorie';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function items(){
    return $this->hasMany('\mywishlist\models\Item','id_categ');
  }
}

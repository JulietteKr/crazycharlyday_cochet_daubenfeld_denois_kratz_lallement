<?php
namespace crazyday\vue;

class VueCompte{

  private $tableau;

  public function __construct($tab=null){
    $this->tableau = $tab;
  }

  public function formulaireCompte(){
    $res = '
    <form id="f1" method="POST" action="./compte">
      <fieldset> <legend> Création de compte</legend>
        <input type="text" name="Login" placeholder="Login" >
        <input type="password" name="MotdePasse" placeholder="Mot de passe" >

        <button type="submit">valider</button>
      </fieldset>
    </form>';

    return $res;
  }

  private function afficherComptes(){
    $res='<section>'. 'Login déjà existants = <br>';
    foreach($this->tableau as $t){
      $res.= $t['nom'].' <input type="button" value="Connexion" name="submit" onclick= "window.location = \'compte/'.$t['id'].'\'"><br><br>';
    }
    $res.='</section>';

    return $res;
  }

  public function authentificationCompte(){
    $res = '
    <form id="f1" method="POST" action="./authentification">
      <fieldset> <legend>  Authentification </legend>
        <input type="text" name="Login" placeholder="Login" >
        <input type="password" name="MotdePasse" placeholder="Mot de passe" >

        <button type="submit">valider</button>
      </fieldset>
    </form>';
    return $res;
  }

  public function buttonsAccueil(){
    $res='';
    if(!(isset($_SESSION['id']))) {
      $res = '<input type="button" value="Se connecter" name="submit" onclick= "window.location = \'authentification\'">';
      $res .= '<input type="button" value="S\'inscrire" name="submit" onclick= "window.location = \'inscription\'">';
      $res .= '<input type="button" value="Afficher categories" name="submit" onclick= "window.location = \'categories\'">';

    }else{
      $res = '
      <form id="deconnexion" method="POST" action="./">
          <input type="submit" name ="deconnexion_post_name" value="Déconnexion"><br>
          </form>';
    }

    return $res;
  }
  public function compte_authentifie(){
    $app = \Slim\Slim::getInstance();
      return 'Vous êtes bien connecté sur le compte de '.$this->tableau['nom'].
            '<br> <img src="'.$app->urlFor('accueil').'img/user/'.$this->tableau['id'].'.jpg"/> <br>';
  }

  public function formulaire_Modifications_Compte(){
    $res = ' <strong>Settings : changer son mot de passe<strong> <br>
    <form id="modifications" method="POST" action="./parametres">
      <fieldset> <legend> '.$this->tableau['login'] .' </legend>
        <label for="label_deconnexion">Actuel
          <input type="password" name="mdp_actuel" placeholder="" >
        </label> <br>

        <label for="label_deconnexion">Nouveau
          <input type="password" name="mdp_nouveau" placeholder="" >
        </label> <br>

        <label for="label_deconnexion">Confirmer
          <input type="password" name="mdp_nouveau_confirmation" placeholder="" >
        </label> <br>

        <button type="submit">Enregistrer les modifications</button>
      </fieldset>
    </form>';
    return $res;
  }

  public function compte_apres_modif(){
    $res = '<strong> Votre mot de passe a bien été modifié ! </strong>
     <br>
     <input type="button" value="Accueil" name="submit" onclick= "window.location = \'./\'">';
    return $res;
  }



  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->buttonsAccueil();
    break;

    case 1 :
    $content = $this->formulaire_Modifications_Compte();
    break;

    case 2 :
    $content = $this->authentificationCompte();
    break;

    case 3 :
    $content = $this->afficherComptes();
    $content.= $this->formulaireCompte();
    break;

    case 4 :
    $content = $this->compte_apres_modif();
    break;

    case 5 :
    $content = $this->compte_authentifie();
    break;


 }
$html = <<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img crazyday="CSS/logo.PNG" alt="Crazy Charly day"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
						        <li><a href="categories">Afficher les categories </a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
echo $html;
}

}

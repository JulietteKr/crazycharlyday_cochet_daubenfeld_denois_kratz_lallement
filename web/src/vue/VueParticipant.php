<?php
namespace crazyday\vue;

class VueParticipant{

  private $tableau;

public function __construct($tab=null){
  $this->tableau = $tab;
}

private function affichercategorieDescategories(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Numero de la categorie : '.$t['id'].'<br>Titre de la categorie  : '.$t['nom'].'<br>Description de la categorie  : '.$t['description'].'<br>';
    $res .= '<input type="button" value="Voir categorie '.$t['id'].'" name="submit" onclick= "window.location = \'categorie/'.$t['id'].'\'"><br>------<br>';
  }
  $res.='</section>';
  $res .= '<form id="creerCateg" method="POST" action="./categories">
  <fieldset>
    <legend>Creation d une nouvelle categorie </legend>
    <label for="f1_name">Titre de la nouvelle categorie : </label>
    <input type="text" id="f1_name" name="categ_voiture" required>
    <label for="f2_name">Description :</label>
    <input type="text" id="f2_name" name="description_catg" required>

    <button type="submit">Créer la catégorie</button></form>
  </fieldset>';

  $res .= '<form id="modifierCateg" method="POST" action="./categories/modif">
  <fieldset>
    <legend>Modification une categorie </legend>
    <label for="f1_name">Numero de la categorie a modifier : </label>
    <input type="number" id="f1_name" name="num_categorie" required>
    <label for="f2_name">Titre :</label>
    <input type="text" id="f2_name" name="categorie">
    <label for="f3_name">Description :</label>
    <input type="text" id="f3_name" name="description_categ">
    <button type="submit">Modifier la categorie </button></form>
  </fieldset>';

  $res .= '<form id="supprimercategorie " method="POST" action="./categories/supp">
  <fieldset>
    <legend>Suppression d\'une categorie </legend>
    <label for="f1_name">Numero de la categorie  a supprimer : </label>
    <input type="number" id="f1_name" name="numerocategorie " required>
    <button type="submit">Supprimer la categorie </button></form>
  </fieldset>';
  return $res;
}

private function afficherItemsDecategorie(){
  $res='<section>';
  //tableau contient une categorie
  $res.= $this->tableau[0]['nom'].'<br> Description : '.$this->tableau[0]['description'].'<br>';
  $items = \crazyday\models\Item::where('id_categ','=',$this->tableau[0]['id'])->get();

  foreach($items as $item){
    $res.= '<br> item : '.$item->nom.'<br> Description : '.'$item->description.';
  }
  $res.='</section>';

  return $res;
}

private function afficherItem(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Item numéro '.$t['id'].'<br> Nom de l\'item = '.$t['nom'].'<br> Description de l\'item = '.$t['descr'].'<br> Participant : '.$t['participant'].'<br>';


      $iditem = $t['id'];

      if($this->tableau[0]['participant'] == NULL) {
          $res .= '<form id="reserver1" method="POST" action="' . $iditem . '">
                 <input type="text" placeholder="<message>" name="message">';

          $res .= '<button type="submit" name="reserver" value="Reserver_reserve1">Réserver</button>
                 </form>';
      }
        $res .= '<form id="supprimer" method="POST" action="./../item/suppr/' . $iditem . '">
                 <button type="submit" name="supprimer" value="Supprimer_supprime">Supprimer</button>
                 </form>';
  }
  return $res.'</section>';
}

private function afficher_formulaire_ajout_image(){
  $res='<section>';
  $res.= 'Item numéro '.$this->tableau['id'].'
  <br> Nom de l\'item = '.$this->tableau['nom'].'
  <br> Description de l\'item = '.$this->tableau['descr'].'
  <br> Participant : '.$this->tableau['participant'].'<br>
  <br> Prix : '.$this->tableau['prix'].'<br>'  ;

  $res .= '<form id="image_item_ajout" method="POST" action="./ajouter">
            <input type="text" placeholder="<url de l\'image>" name="img_item_new">';

  $res.='<button type="submit" name="valider_image" value="valider_image1">Valider</button>
         </form>
         </section>';
  return $res;
}

private function afficher_formulaire_modif_image(){
  $res='<section>';
  $res.= 'Item numéro '.$this->tableau[0]['id'].'
          <br> Nom de l\'item = '.$this->tableau[0]['nom'].'
          <br> Description de l\'item = '.$this->tableau[0]['descr'].'
          <br> Participant : '.$this->tableau[0]['participant'].'<br>';

  $res .= '<form id="image_item_modif" method="POST" action="./modifier">
            <input type="text" placeholder="<url de l\'image>" name="img_item_modif">
            <button type="submit" name="valider_image" value="valider_image1">Valider la modification</button>
            <br><br>Image actuelle : <img crazyday="'.$this->tableau[1]['url'].'"';

  $res.='
         </form>
         </section>';
  return $res;
}

private function afficher_formulaire_modif_item(){
  $res=$this->href_item_createur();
  $res.='<section>';

  $res.= 'Item numéro '.$this->tableau[0]['id'].'
          <br> Nom de l\'item = '.$this->tableau[0]['nom'].'
          <br> Description de l\'item = '.$this->tableau[0]['descr'].'
          <br> Participant : '.$this->tableau[0]['participant'].'<br>';

  $res .= '<form id="item_modif" method="POST" action="./../../categoriesCreateur">
            <input type="text" placeholder="<Nom>" name="nom_item_modif">
            <input type="text" placeholder="<Description>" name="descr_item_modif">
            <input type="number" placeholder="<Tarif>" name="tarif_item_modif">
            <button type="submit" name="valider_image" value="valider_image1">Valider la modification</button>';

  $res.='
         </form>
         </section>';
  return $res;
}

public function render(int $selecteur) {
switch ($selecteur) {
 case 0 :
 $content = $this->affichercategorieDescategories();
 break;

 case 1 :
 $content = $this->afficherItemsDecategorie();
 break;

 case 2 :
 $content = $this->afficherItem();
 break;

 case 3 :
 $content = $this->afficher_formulaire_ajout_image();
 break;

 case 4 :
 $content = $this->afficher_formulaire_modif_image();
 break;

 case 5 :
 $content = $this->afficher_formulaire_modif_item();
 break;
}
$html = <<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img crazyday="CSS/logo.PNG" alt="Crazy Charly day"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
						        <li><a href="categories">Afficher les categories </a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
echo $html;
}

}

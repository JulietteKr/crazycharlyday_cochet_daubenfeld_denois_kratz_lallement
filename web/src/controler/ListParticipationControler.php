<?php
namespace crazyday\controler;

use \crazyday\models\Categorie;
use \crazyday\models\Item;
use \crazyday\models\Compte;
use \crazyday\vue\VueCreateur;
use \crazyday\vue\VueParticipant;
use \crazyday\vue\VueErreur;

class ListParticipationControler{

  public function categories(){
      echo "categories ";
  }

  public function affichercategorieDescategories(){
    $categories  = Categorie::get()->toArray();
    $vue = new VueParticipant($categories );
    $vue->render(0);
  }

  public function afficherItemsDeLacategorie($idcategories){
    $categories  = Categorie::find($idcategories)->toArray();
    $vue = new VueParticipant([$categories]);
    $vue->render(1);
  }

  public function afficherItem($id){
    $item = Item::find($id)->toArray();
    $vue = new VueParticipant([$item]);
    $vue->render(2);
  }

  public function reserver($id){
    $item = Item::find($id);
    $user = Compte::find($_SESSION['user_id']);
    $item->participant = $user->login;
    $item->message = $_POST['message'];
    $item->save();
    $this->afficherItem($id);
  }

  public function supprimerItemDecategories($id){
      $item = Item::find($id);
      if($item->participant != NULL){
          $item->participant = NULL;
          $item->save();
      }
      $this->afficherItemReserve();
  }

  public function afficherItemReserve(){
    $user = Compte::find($_SESSION['user_id']);
    $categories  = Item::where("participant","=",$user->login)->get()->toArray();

    $vue = new VueParticipant($categories );
    $vue->render(2);

  }

  public function modifiercategories($idcategories ){
    $categories  = Categorie::find($idcategories );
    if($_SESSION['user_id']=$categories ->user_id){
      if(isset($_POST['titrecategories '])){
        $categories ->titre = $_POST['titrecategories '];
      }
      if(isset($_POST['descrcategories '])){
        $categories ->description = $_POST['descrcategories '];
      }
      if(isset($_POST['expiration'])){
        $categories ->expiration = $_POST['expiration'];
      }
      $categories ->save();

      $categories  = Categorie::all()->toArray();
      $vue = new VueParticipant($categories );
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

  public function supprimercategories($idcategories){
    $categories  = Categorie::find($idcategories );
    if($_SESSION['user_id']=$categories ->user_id){
      $categories ->delete();

      $categories  = categories ::all()->toArray();
      $vue = new VueParticipant($categories );
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

  public function affichercategoriesDuCreateur(){
    $categories  = null;

    if( isset($_SESSION['user_id']) ){

      $categories  = Categorie::where('user_id','=',$_SESSION['user_id'])->get()->toArray();
      $vue = new VueCreateur($categories );
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

}

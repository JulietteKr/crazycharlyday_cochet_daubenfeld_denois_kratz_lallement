<?php
namespace crazyday\controler;

use \crazyday\vue\VueParticipant;
use \crazyday\vue\VueErreur;
use \crazyday\vue\VueCompte;
use \crazyday\models\Categorie ;
use \crazyday\models\Compte;

class CompteControler{

  public function creerCompteControl(){
    $util = new \crazyday\models\Compte();

    $nom=$_POST['nom'];
    $mdp=$_POST['MotdePasse'];
    $hash=password_hash($mdp, PASSWORD_DEFAULT, ['cost'=> 12] );
    //$util = new \crazyday\models\Compte($nom, $hash);
    $util->nom=$nom;
    $util->mdp=$hash;
    $util->save();

    $this->afficherLesComptes();
  }

  public function authentifierCompte(){

    $nom=$_POST['nom'];

    $util =\crazyday\models\Compte::where("nom","=",$nom)->first();


    $mdp=$_POST['MotdePasse'];

    if (password_verify($mdp, $util->mdp)){
      echo "utilisateur connecté !";
      $_SESSION['id']= $util->id_compte;
      $categorie  = categorie ::where('id','=',$_SESSION['id'])->get()->toArray();
      $vue = new VueCompte($categorie );
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
    //$util = new \crazyday\models\Compte($nom, $hash);
  }

  public function sauvegarderConnexion($idcompte){

    $util =Compte::where("id","=",$idcompte)->first();
      $_SESSION['id']= $util->id;
      $vue = new VueCompte($util);
      $vue->render(5);

    //$util = new \crazyday\models\Compte($nom, $hash);
  }

  public function formulaireConnexion(){

    $vueC = new VueCompte();
    $vueC->render(2);
  }

  public function afficherButtonsAccueil(){
    $vueC = new VueCompte();
    $vueC->render(0);
  }

  // public function deconnecter_utilisateur(){
  //   // if (isset($_GET['deconnexion_post_name'])){
  //   //   session_destroy();
  //   // }
  //   $vueC = new \crazyday\vue\VueCompte();
  //   $vueC->render(0);
  // }

  public function afficher_Modifications(){
    $util = Compte::where("id_compte","=",$_SESSION['id'])->first()->toArray();
    $vue = new VueCompte($util);
    $vue->render(1);
  }

  public function post_Modifications(){

    if(($_POST['mdp_nouveau'] == $_POST['mdp_nouveau_confirmation'])){
      $vue = new VueCompte();
      $vue->render(4);
    }else{
      $vue = new VueErreur();
      $vue->render(1);
    }

  }

  public function afficherLesComptes(){
    $comptes = Compte::get()->toArray();
    $vueC = new VueCompte($comptes);
    $vueC->render(3);

  }






}

<?php

namespace crazyday\controler;

use \crazyday\models\Categorie;

class ListCreateControler{

  public function creerCategorie(){

    $nvcategorie  = new Categorie();
    $nvcategorie ->user_id =$_SESSION['user_id'];
    $nvcategorie ->titre = $_POST['titrecategorie '];
    $nvcategorie ->description = $_POST['descrcategorie '];
    $nvcategorie ->expiration = $_POST['expiration'];
    $nvcategorie ->save();

    $categorie  = Categorie::where("user_id","=",$_SESSION['user_id'])->get()->toArray();
    $vue = new \crazyday\vue\VueCreateur($categorie );
    $vue->render(0);

  }

  public function ajouterItem($idcategorie ){
    $categorie  = Categorie::find($idcategorie );

    if($categorie ->user_id == $_SESSION['user_id']){
      $item = new \crazyday\models\Item();
      $item->categorie _id = $idcategorie ;
      $item->nom = $_POST['nom'];
      $item->descr = $_POST['description'];
      $item->tarif = $_POST['prix'];
      $item->save();
    }

    $categorie  = Categorie::find($idcategorie )->toArray();
    $vue = new \crazyday\vue\VueParticipant([$categorie ]);
    $vue->render(1);

  }

}

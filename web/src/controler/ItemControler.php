<?php

namespace crazyday\controler;


use \crazyday\vue\VueCreateur;
use \crazyday\vue\VueParticipant;
use \crazyday\models\Categorie ;
use \crazyday\models\Item;
use \crazyday\models\Image;

class ItemControler{

public function afficherItems_Avec_Button_Image($idcategorie ){
  $this->items_correspondant_idcreateur($idcategorie );
}

public function ajouter_item_categorie_Createur($idcategorie ){
  $item = new Item();

  $nom_item = $_POST['nom_item'];
  $descr_item = $_POST['description_item'];
  $prix_item = $_POST['prix_item'];

  $item->nom = filter_var($nom_item,FILTER_SANITIZE_STRING);
  $item->descr = filter_var($descr_item,FILTER_SANITIZE_STRING);

  if(filter_var($prix_item,FILTER_VALIDATE_FLOAT)){
      $item->tarif = $prix_item;
  }

  $item->categorie _id = $idcategorie ;
  $item->save();

  $this->items_correspondant_idcreateur($idcategorie );
}

public function supprimer_item_categorie Crea($idcategorie ){
  $item = Item::find($idcategorie );
  if($item->participant != NULL){
      $item->participant = NULL;
      $item->save();
  }
  $this->items_correspondant_idcreateur($idcategorie );
}

public function items_correspondant_idcreateur($idcategorie ){
  $items = Item::where('categorie _id','=',$idcategorie )->get()->toArray();
  $vue = new VueCreateur($items);
  $vue->render(1);
}

public function formulaire_modif_item($iditem){

  $item = Item::find($iditem)->first()->toArray();
  $res_tableau[] = $item;
  $vue =  new VueParticipant($res_tableau);
  $vue->render(5);

}

public function afficher_modif_item($iditem){

  $item = Item::find($iditem)->toArray();

  $item->nom = $_POST['nom_item_modif'];
  $item->descr = $_POST['descr_item_modif'];
  $item->tarif = $_POST['tarif_item_modif'];

  $res_tableau[] = $item;

  $item->save();

  $res_tableau[] = $item;

  $this->afficherItems_Avec_Button_Image();
}

}
